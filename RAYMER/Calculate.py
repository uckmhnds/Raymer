

class Calculate(object):

    def __defaults__(self):

        self.step               = 1000
        self.tolerance          = 1e-5
        self.human              = 80
        self.stepSign           = 0
        self.stepSignOld        = 0
        self.gravity            = 9.81

    def __init__(self, aircraft, fuel, empty):

        self.__defaults__()

        self.weightGuess        = aircraft.weightGuess
        self.payload            = aircraft.payload
        self.humanNo            = aircraft.humanNo

        self.fuelFraction       = fuel.fraction

        self.A                  = empty.A
        self.C                  = empty.C

        self.human              = self.humanNo * self.human

        while True:

            self.__call__()

            if abs((self.weightGuess - self.weightCalculated) / self.weightCalculated) < self.tolerance:

                self.emptyWeight    = self.A * pow(self.weightCalculated, self.C) * self.weightCalculated
                self.fuelWeight     = self.fuelFraction * self.weightCalculated

                break

    def __call__(self, *args, **kwargs):

        self.weightCalculated   = (self.human + self.payload) / (1 - self.fuelFraction - self.A * pow(self.weightGuess, self.C))

        if self.weightCalculated > self.weightGuess:
            self.weightGuess    += self.step
            self.stepSign       = 1

        else:
            self.weightGuess    -= self.step
            self.stepSign       = -1

        if self.stepSign * self.stepSignOld == -1:
            self.weightGuess    = self.weightGuess + self.stepSignOld * self.step / 2.
            self.step           /= 10.
            self.stepSign       = 0

        self.stepSignOld        = self.stepSign