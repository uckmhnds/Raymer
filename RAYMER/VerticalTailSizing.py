import numpy as np
class VerticalTailSizing(object):

    def __defaults__(self):

        # Table 6.4 Tail Volume Coefficient

        if self.tag     == "twin_turboprop":

            self.cvt                    = 0.08
            self.arUpperBand            = 2.
            self.arLowerBand            = 1.3
            self.taperUpper             = 0.6
            self.taperLower             = 0.3

        elif self.tag   == "jet_trainer":

            self.cvt                    = 0.06
            self.arUpperBand            = 2.
            self.arLowerBand            = 1.3
            self.taperUpper             = 0.6
            self.taperLower             = 0.3

        elif self.tag   == "jet_fighter_dogfighter":

            self.cvt                    = 0.07
            self.arUpperBand            = 1.4
            self.arLowerBand            = 0.6
            self.taperUpper             = 0.4
            self.taperLower             = 0.2

        elif self.tag   == "jet_fighter_other":

            self.cvt                    = 0.07
            self.arUpperBand            = 1.4
            self.arLowerBand            = 0.6
            self.taperUpper             = 0.4
            self.taperLower             = 0.2

        elif self.tag   == "military_cargo":

            self.cvt                    = 0.08
            self.arUpperBand            = 2.
            self.arLowerBand            = 1.3
            self.taperUpper             = 0.6
            self.taperLower             = 0.3

        elif self.tag   == "military_bomber":

            self.cht                    = 1.
            self.cvt                    = 0.08
            self.arUpperBand            = 2.
            self.arLowerBand            = 1.3
            self.taperUpper             = 0.6
            self.taperLower             = 0.3

    def __init__(self, tag, wingSpan, wingArea, vtArm):

        self.tag                = tag

        self.__defaults__()

        self.area               = self.cvt * wingArea * wingSpan / vtArm
        # Table 4.3 is averaged
        self.AR                 = self.arLowerBand + 0.5 * (self.arUpperBand - self.arLowerBand)
        self.taperRatio         = self.taperLower + 0.5 * (self.taperUpper - self.taperLower)
        self.span               = np.sqrt(self.area * self.AR)
        self.rootChord          = 2 * self.area / self.span / (1 + self.taperRatio)
        self.tipChord           = self.taperRatio * self.rootChord
        self.meanChord          = 2 / 3. * self.rootChord * (1 + self.taperRatio + self.taperRatio * self.taperRatio) / (1 + self.taperRatio)
        self.meanChordOffset    = self.span / 6. * (1 + 2 * self.taperRatio) / (1 + self.taperRatio)