
class EmptyFraction(object):  # taken from parameters defined in Table 3.1

    def __defaults__(self):

        if self.tag == "twin_turboprop":
            self.A = 0.92
            self.C = -0.05

        elif self.tag == "jet_trainer":
            self.A = 1.47
            self.C = -0.1

        elif self.tag == "jet_fighter_dogfighter":
            self.A = 2.11
            self.C = -0.13

        elif self.tag == "jet_fighter_other":
            self.A = 2.11
            self.C = -0.13

        elif self.tag == "military_cargo":
            self.A = 0.88
            self.C = -0.07

        elif self.tag == "military_bomber":
            self.A = 0.88
            self.C = -0.07

    def __init__(self, tag):

        self.tag        = tag

        self.__defaults__()