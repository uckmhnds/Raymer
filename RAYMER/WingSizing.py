import numpy as np
from scipy import interpolate

class WingSizing(object):

    def __defaults__(self):

        if self.tag == 'jet_trainer':

            self.a = 4.737
            self.C = -0.979

        elif self.tag == 'jet_fighter_dogfighter':

            self.a = 5.416
            self.C = -0.622

        elif self.tag == 'jet_fighter_other':

            self.a = 4.11
            self.C = -0.622

        elif self.tag == 'military_cargo':

            self.a = 5.57
            self.C = -1.075

        elif self.tag == 'military_bomber':

            self.a = 5.57
            self.C = -1.075

        # Digitized from Raymer Fig 4.14 Thickness ratio historical trend

        self.thicknessToChordSubMach        = [0.10320354892731565, 0.20158340327277924, 0.345647574809423, 0.42999261284745705,
                                                0.5599848389330012, 0.6759793778547861, 0.8130323294283269, 0.9816976527419484, 0.966873842131522]

        self.thicknessToChordSubThickness   = [0.15500364480128134, 0.1526459203893264, 0.1493436277579852, 0.147689606669473, 0.14439059949280794,
                                               0.14249571350835225, 0.13966200885019353, 0.13588702142732473, 0.05534143061017055]

        self.thicknessToChordSupMach        = [1.096816562692172, 1.2127615960890634, 1.3357302258302806, 1.4938910955804716, 1.6098856345022567,
                                               1.7364465088935903, 1.8489478142150473, 1.954425523192178, 2.0634335949133074, 2.154864111201785, 2.274450894773686,
                                               2.4537939409878673, 2.682413549043344, 2.914575896224043, 3.0869696043812396, 3.2839768406966363, 3.4598637824541596,
                                               3.593547264239607, 3.7976276024242237, 4.001707940608841]

        self.thicknessToChordSupThickness   = [0.051108532941816665, 0.04827975646567215, 0.045215864639267356, 0.04237723179909447, 0.040482345814638776, 0.03881846836209818,
                                               0.03739134898715593, 0.036199344962473956, 0.035239992197045156, 0.03451821887288373, 0.03402334726229225, 0.03258062197763839,
                                               0.03159334284746254, 0.031072187599462037, 0.031265413402601666, 0.031219417037135905, 0.03094487623076214, 0.030913664411338942,
                                               0.03156644318730173, 0.032219221963264544]

        # Digitized from Raymer Fig 4.20 Wing Sweep historical trend

        self.leadingEdgeSweepMach           = [0.2609641568895176, 0.3666032438182162, 0.4439236530938663, 0.513984776961255, 0.56962996772355, 0.5972301813966038, 0.6279810617871248,
                                               0.6692445320148016, 0.7175620122278837, 0.7655031541657303, 0.8068692621048349, 0.8447767901921891, 0.903675285383379, 0.9769555011091872,
                                               1.0397915521387908, 1.1062572458725257, 1.193748119280572, 1.2883614278149755, 1.3725305716531748, 1.4357087483875381, 1.5480255070264062,
                                               1.6887293684592755, 1.8046757698022748, 1.9277788188421066, 2.0614286702895703, 2.18463435704083, 2.3324264410295807, 2.4627203503367214,
                                               2.6282500970004126, 2.7550854264558033, 2.867846948510859, 2.9664662380246285, 3.103951007599079, 3.2555095845738635, 3.4176833890972316,
                                               3.562222168657087, 3.703302368365191, 3.85845637547363, 3.953685510276601, 4.003029367603962]

        self.leadingEdgeSweepAngle          = [0.20233448665067044, 0.8255413243837637, 1.7929660721581087, 3.464822282183434, 6.19218313971605, 8.321473168637453, 11.747552759075162,
                                               15.4126321717835, 19.079996676230195, 24.043008196323996, 27.354729332037678, 30.429735737219048, 34.10052787927329, 36.83360146615178,
                                               39.092103046096526, 40.99838889491578, 43.382674388276314, 45.533672788712096, 47.21009918221413, 48.29073983884332, 50.21187878396187,
                                               51.55322763437608, 53.12215084836915, 54.34000087710591, 55.67906463578175, 56.54355638752385, 57.769404237344844, 58.51839498849374,
                                               59.39659729066597, 59.90887331128257, 60.29879305609091, 60.801928709754094, 61.08206018331513, 61.48454793268442, 61.77267722732969,
                                               62.055093792629066, 62.10079562739616, 62.26885373797152, 62.29970247643931, 62.43348421093934]

        # Digitized from Raymer Fig 4.24 Effect of sweep on desired taper ratio

        self.quarterChordSweepSweep         = [0.6197631605134859, 4.044183500845875, 9.618867549009849, 15.821673798387906, 24.1691710617972, 35.41964374564634, 46.15782664941787,
                                               60.17673400338343, 69.89750223902875, 80.12180316449398]

        self.quarterChordSweepTaper         = [0.442319436075174, 0.38601997031548896, 0.3131230613061995, 0.24922047826843052, 0.18666866181091712, 0.12164583223802605, 0.08096511058895861,
                                               0.040389538434162375, 0.025316762851492536, 0.014106209077636711]

    def __init__(self, tag, maxMach, wingArea):

        self.tag            = tag
        self.designMach     = maxMach
        self.wingArea       = wingArea

        self.__defaults__()

        self.AR = self.a * pow(self.designMach, self.C)

        if self.tag == 'twin_turboprop':
            self.AR = 9.2

        self.span = np.sqrt(self.AR * self.wingArea)

        # Find Thickness to Chord Ratio from Historical Trend

        if self.designMach < 1.:
            thicknessInterpolate = interpolate.interp1d(self.thicknessToChordSubMach, self.thicknessToChordSubThickness)

        else:
            thicknessInterpolate = interpolate.interp1d(self.thicknessToChordSupMach, self.thicknessToChordSupThickness)

        self.thicknessToChordRatio = thicknessInterpolate(self.designMach)

        # Find Leading Edge Sweep from Historical Trend

        wingSweep               = interpolate.interp1d(self.leadingEdgeSweepMach, self.leadingEdgeSweepAngle)

        self.leadingEdgeSweep   = wingSweep(self.designMach)

        # Find Quarter Chord Sweep and Taper Ratio numerically

        self.__sweep_numerics__()

        self.rootChord          = 2 * self.wingArea / (self.span * (1 + self.taperRatio))
        self.tipChord           = self.taperRatio * self.rootChord
        self.meanChord          = self.rootChord * (1 + self.taperRatio + self.taperRatio * self.taperRatio) / (1 + self.taperRatio) * 2. / 3.
        self.meanChordOffset    = self.span * (1 + 2 * self.taperRatio) / (1 + self.taperRatio) / 6.

    def __sweep_numerics__(self):

        # Find quarter chord sweep and taper from Equation in Fig 4.16 and Trendline in Fig 4.24 numerically

        taperInterpolate        = interpolate.interp1d(self.quarterChordSweepSweep, self.quarterChordSweepTaper)

        quarterSweepGuess       = self.leadingEdgeSweep * 0.8

        for i in range(1000):

            taperGuess = taperInterpolate(quarterSweepGuess)

            quarterCalculated       = np.degrees(np.arctan(np.tan(np.radians(self.leadingEdgeSweep)) - (1 - taperGuess) / (1 + taperGuess) / self.AR))

            quarterSweepGuess       = (quarterSweepGuess + quarterCalculated) * 0.5

            if abs((quarterCalculated - quarterSweepGuess) / quarterCalculated) < 1e-4:

                self.quarterChordSweep      = quarterCalculated
                self.taperRatio             = taperInterpolate(self.quarterChordSweep)

                break