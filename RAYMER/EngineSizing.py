import numpy as np

class EngineSizing(object):

    def __defaults__(self):

        # SSD0000MET001.docx

        # N / cm2

        if self.tag     == "subsonic_low":
            self.thrustDensity      = 3

        elif self.tag   == "subsonic_mid":
            self.thrustDensity      = 5

        elif self.tag   == "supersonic_twin":
            self.thrustDensity      = 6.5

        elif self.tag   == "supersonic_single":
            self.thrustDensity      = 7.5

    def __init__(self, tag, thrust):

        self.tag            = tag
        self.__defaults__()

        self.diameter       = np.sqrt(thrust / self.thrustDensity * 1e-4) * 4 / np.pi