


class WingLoading(object):

    def __defaults__(self):

        # Table 5.5 Wing Loading (kg/m2)

        if self.tag     == "twin_turboprop":
            self.wingLoading    = 195

        elif self.tag   == "jet_trainer":
            self.wingLoading    = 244

        elif self.tag   == "jet_fighter":
            self.wingLoading    = 342

        elif self.tag   == "military_bomber":
            self.wingLoading    = 586

    def __init__(self, tag):

        self.tag        = tag

        self.__defaults__()