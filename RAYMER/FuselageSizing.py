from scipy import interpolate
import numpy as np
class FuselageSizing(object):

    def __defaults__(self):

        # Table 6.3 Fuselage Length vs W0

        if self.tag     == "twin_turboprop":
            self.a      = 0.169
            self.C      = 0.51

        elif self.tag   == "jet_trainer":
            self.a      = 0.333
            self.C      = 0.41

        elif self.tag   == "jet_fighter_dogfighter":
            self.a      = 0.389
            self.C      = 0.39

        elif self.tag   == "jet_fighter_other":
            self.a      = 0.389
            self.C      = 0.39

        elif self.tag   == "military_cargo":
            self.a      = 0.104
            self.C      = 0.5

        elif self.tag   == "military_bomber":
            self.a      = 0.104
            self.C      = 0.5

        self.finenessInterpolate    = [3, 14]
        self.machInterpolate        = [0.1, 1.4]

    def __init__(self, tag, weight, designMach):

        self.tag            = tag

        self.__defaults__()

        self.length         = self.a * pow(weight, self.C)


        # Fuselage Fineness Ratio (self.length / self.diameter) is
        # 3 for subsonic    (M=0.1, assume),
        # 14 for supersonic (M=1.4, assume)

        interpolateFinenes  = interpolate.interp1d(self.machInterpolate, self.finenessInterpolate)
        self.finenessRatio  = interpolateFinenes(designMach)

        self.diameter       = self.length / self.finenessRatio
        self.projectedArea  = np.pi * self.diameter * self.diameter * 0.25
