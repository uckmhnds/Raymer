import numpy as np

class FuelFraction(object):

    def __defaults__(self):

        self.fraction           = 1.
        self.missionTakeOff     = 0.97
        self.missionClimb       = 0.985
        self.missionLanding     = 0.995

    def __init__(self, missions):

        self.__defaults__()

        for mission in missions:

            if mission.tag      == "takeOff":
                self.missionFraction    = self.missionTakeOff

            elif mission.tag    == "climb":
                self.missionFraction    = self.missionClimb

            elif mission.tag    == "landing":
                self.missionFraction    = self.missionLanding

            elif mission.tag    == "cruise":
                self.missionFraction    = np.exp(-mission.distance * mission.fuelConsumption / mission.air_speed / mission.LoverD)

            elif mission.tag    == "loiter":
                self.missionFraction    = np.exp(-mission.endurance * mission.fuelConsumption / mission.LoverD)

            self.__call__()

        self.fraction   = 1.06 * (1 - self.fraction)

    def __call__(self, *args, **kwargs):

        self.fraction   *= self.missionFraction
