import numpy as np

class AtmosphericConditions(object):

    def __init__(self, atmosphere):

        self.altitude                   = atmosphere["altitude"]
        self.altitudeFT                 = self.altitude / 0.3048
        self.ISA                        = atmosphere["ISA"]
        self.gamma                      = atmosphere["gamma"]
        self.R                          = atmosphere["R"]
        self.gravity                    = 9.81
        self.seaLevelPressure           = 101300.

        if self.altitudeFT > 36089.:

            self.temperature            = self.ISA - 56.5 + 273.15
            self.pressure               = 0.22336 * np.exp(-0.0000480634 * (self.altitudeFT - 36089)) * self.seaLevelPressure

        else:

            self.temperature            = (1 - 0.00000687535 * self.altitudeFT) * 288.15 + self.ISA
            self.pressure               = pow((1 - 0.00000687535 * self.altitudeFT), 5.2561) * self.seaLevelPressure

        self.density                    = self.pressure / self.temperature / self.R
        self.soundSpeed                 = np.sqrt(self.gamma * self.R * self.temperature)

    def update(self, newAltitude):

        self.altitude                   = newAltitude
        self.altitudeFT                 = self.altitude / 0.3048
        self.ISA                        = self.ISA
        self.gamma                      = self.gamma
        self.R                          = self.R
        self.seaLevelPressure           = 101300.

        if self.altitudeFT > 36089.:

            self.temperature            = self.ISA - 56.5 + 273.15
            self.pressure               = 0.22336 * np.exp(-0.0000480634 * (self.altitudeFT - 36089)) * self.seaLevelPressure

        else:

            self.temperature            = (1 - 0.00000687535 * self.altitudeFT) * 288.15 + self.ISA
            self.pressure               = pow((1 - 0.00000687535 * self.altitudeFT), 5.2561) * self.seaLevelPressure

        self.density                    = self.pressure / self.temperature / self.R
        self.soundSpeed                 = np.sqrt(self.gamma * self.R * self.temperature)
