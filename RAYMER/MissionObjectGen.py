


class MissionObjectGen(object):

    def __init__(self, mission):

        self.tag                    = mission[0]

        if self.tag                 == "cruise":

            self.distance           = mission[1]
            self.fuelConsumption    = mission[2]
            self.air_speed          = mission[3]
            self.LoverD             = mission[4]

        elif self.tag               == "loiter":

            self.endurance          = mission[1]
            self.fuelConsumption    = mission[2]
            self.LoverD             = mission[3]