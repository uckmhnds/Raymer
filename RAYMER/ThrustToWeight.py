

class ThrustToWeight(object):

    def __defaults__(self):

        # Table 5.3 T/W vs MachMax

        if self.tag     == "jet_trainer":
            self.a      = 0.488
            self.C      = 0.728
        elif self.tag   == "jet_fighter_dogfighter":
            self.a      = 0.648
            self.C      = 0.594
        elif self.tag   == "jet_fighter_other":
            self.a      = 0.514
            self.C      = 0.141
        elif self.tag   == "military_cargo":
            self.a      = 0.244
            self.C      = 0.341
        elif self.tag   == "military_bomber":
            self.a      = 0.244
            self.C      = 0.341

    def __init__(self, tag, maxMach):

        self.tag                = tag

        self.__defaults__()

        self.thrustToWeight     = self.a * pow(maxMach, self.C)